from flask import Flask, request, redirect, url_for

ALLOWED_EXTENSIONS = {'txt'}

app = Flask(__name__)


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1] in ALLOWED_EXTENSIONS


@app.route("/", methods=['GET', 'POST'])
def index():
    lines_as_html = "Please upload a dungeon log"
    if request.method == 'POST':
        file = request.files['file']
        if file and allowed_file(file.filename):
            lines = file.read().decode('utf-8')
            lines_as_html = lines.replace('\n', '<br>')
            # return redirect(url_for('index'))
    return """
    <!doctype html>
    <title>ITRTG Dungeon Log Parser</title>
    <p>Idling to Rule the Gods is an awesome incremental game made by Denny Stöhr. You can play it for free on Android, Kongregate, and Steam.</p>
    <p>This website allows you to upload Dungeon Logs from ITRTG and compute fancy graphs for them.</p>
    <h1>Upload a Dungeon Log File</h1>
    <form action="" method=post enctype=multipart/form-data>
      <p><input type=file name=file>
         <input type=submit value=Upload>
    </form>
    <h1>Uploaded RAW</h1>
    <p>%s</p>
    """ % lines_as_html


if __name__ == "__main__":
    app.run(host='0.0.0.0', port=5001, debug=True)
